const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/hype-serials.pug',
		filename: './hype-serials.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/new-serials.pug',
		filename: './new-serials.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/filter.pug',
		filename: './filter.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/random.pug',
		filename: './random.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/search.pug',
		filename: './search.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/serial.pug',
		filename: './serial.html'
	}),
]