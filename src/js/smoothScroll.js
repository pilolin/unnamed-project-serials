import smoothscroll from 'smoothscroll-polyfill';

smoothscroll.polyfill();

const delta = 15; // отступ от хедера (px)

function scrollPageToElement(idElement) {
	const element = document.querySelector(idElement);
	const heightHeader = document.getElementById('header').clientHeight;
	let scroll = element.getBoundingClientRect().top + window.scrollY - heightHeader - delta;

	if (element.closest('[role="tablist"]') && element.getAttribute('aria-hidden') === 'true' ) {
		const tabs = element.closest('[role="tablist"]');
		const targetTab = tabs.querySelector(`[href="${idElement}"][role="tab"]`);
		const activeTab = tabs.querySelector(`[role="tab"][aria-selected="true"]`);

		targetTab.setAttribute('aria-selected', 'true');
		activeTab.setAttribute('aria-selected', 'false');
	}

	if (element.closest('[role="tablist"]')) {
		scroll = element.closest('[role="tablist"]').getBoundingClientRect().top + window.scrollY - heightHeader - delta;
	}

	setTimeout(() => {
		window.scrollTo({top: scroll, behavior: 'smooth'});
	}, 100);
}

document.addEventListener('click', e => {
	const link = e.target.closest('a');

	if( !link ) return 0;

	const linkHash = link.getAttribute('href').replace(/^(\/)/, '');

	if(linkHash[0] === '#' && linkHash.length > 1) {

		if( link.getAttribute('href')[0] === '/' && link.getAttribute('href').indexOf(window.location.pathname) === -1 ) return 0;

		scrollPageToElement(linkHash);

		e.preventDefault();
	}
});

window.addEventListener('load', () => {
	if(window.location.hash.length > 1) {
		scrollPageToElement(window.location.hash);
	}
})