import {isTouchDevice} from './helper';

if (document.querySelector('.serial-rating-vote')) {
	// open vote bar
	document.querySelector('.serial-rating-vote__open-vote-bar').addEventListener('click', (e) => {
		const voteBar = e.currentTarget.nextElementSibling;
	
		voteBar.setAttribute('aria-hidden', 'false');
	
		e.preventDefault();
	});
	
	// close vote bar
	document.querySelector('.serial-rating-vote__bar-close').addEventListener('click', (e) => {
		const voteBar = e.currentTarget.parentElement;
	
		voteBar.setAttribute('aria-hidden', 'true');
	
		e.preventDefault();
	});
}

// hover stars
document.addEventListener('mouseover', function(e) {
	const target = e.target;

	if( target.closest('.serial-rating-vote__bar-stars a') && !isTouchDevice()) {
		const ratingWrap = target.closest('.serial-rating-vote__bar-stars');
		const currentStar = target.closest('.serial-rating-vote__bar-stars a');
		const listStar = ratingWrap.children;
		let hoverStar = true;

		for (const star of [...listStar]) {
			if(hoverStar) {
				star.classList.add('hover');
			} else {
				star.classList.remove('hover');
			}

			if( hoverStar && star === currentStar ) {
				hoverStar = false;
			}
		}
	}
});

document.addEventListener('mouseout', function(e) {
	const target = e.target;

	if( target.closest('.serial-rating-vote__bar-stars') && !isTouchDevice()) {
		const ratingWrap = target.closest('.serial-rating-vote__bar-stars');

		if( ratingWrap ) {
			const listStar = ratingWrap.children;
	
			for (const i in [...listStar]) {
				const star = listStar[i];

				star.classList.remove('hover');
			}
		}
	}
});

// voting
document.addEventListener('click', function(e) {
	const target = e.target;

	if( target.closest('.serial-rating-vote__bar-stars a')) {
		const ratingWrap = target.closest('.serial-rating-vote__bar-stars');
		const currentStar = target.closest('.serial-rating-vote__bar-stars a');
		const listStar = ratingWrap.children;
		const voteBar = target.closest('.serial-rating-vote__bar');
		let hoverStar = true;

		for (const star of [...listStar]) {
			if(hoverStar) {
				star.classList.add('hover');
			} else {
				star.classList.remove('hover');
			}

			if( hoverStar && star === currentStar ) {
				hoverStar = false;
			}
		}

		setTimeout(() => {
			for (const star of [...listStar]) {
				star.classList.remove('hover');
			}
		
			voteBar.setAttribute('aria-hidden', 'true');
		}, 150);
	}
});