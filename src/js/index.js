import SimpleBar from'simplebar';
import {isTouchDevice} from './helper';
import SnowMoreText from './show-more';
import './smoothScroll';
import './filter';
import './modal';
import './tabs';
import './vote';
import './serial-trailer';
import './search-header';
import './info-popover/info-popover';
import './../scss/styles.scss';

window.SimpleBar = SimpleBar;

new SnowMoreText(document.querySelector('.serial-info__description'));

document.addEventListener('DOMContentLoaded', () => {
	if(isTouchDevice) {
		document.body.classList.add('touch');
	} else {
		document.body.classList.add('no-touch');
	}
});

document.addEventListener('click', (e) => {
	if (!e.target.closest('.dropdown__header')) return;

	const dropdown = e.target.closest('.dropdown');
	const dropdownContent = dropdown.querySelector('.dropdown__content');
	const isOpen = dropdown.getAttribute('aria-expanded') === 'true';
	const group = dropdown.dataset.group;

	// close all opened dropdowns with similar group
	if (group) {
		const groupOpenedDropdowns = document.querySelectorAll(`.dropdown[data-group="${group}"][aria-expanded="true"]`);

		for (const d of groupOpenedDropdowns) {
			d.setAttribute('aria-expanded', 'false');
			d.querySelector('.dropdown__content').style.height = '0px';
		}
	}

	if (isOpen) {
		dropdown.setAttribute('aria-expanded', 'false');
		dropdownContent.style.height = '0px';
	} else {
		dropdown.setAttribute('aria-expanded', 'true');
		dropdownContent.style.height = dropdownContent.scrollHeight + 'px';
	}

	e.preventDefault();
});