import Swal from 'sweetalert2/dist/sweetalert2.js';
import CustomSelect from './select';

const filterModal = Swal.mixin({
	showConfirmButton: false,
	showCloseButton: true,
	closeButtonHtml: '<i class="icon-close"></i>',
	showClass: {
		popup: 'animated fadeInUp fast'
	},
	hideClass: {
		popup: 'animated fadeOutUp fast'
	},
	customClass: {
		container: 'swal2-filter',
	}
});

// open modal filter
document.addEventListener('click', (e) => {
	const target = e.target;

	if( !target.closest('.header__btn-filter[data-filter-modal]') ) return;

	const button = target.closest('button');
	const header = document.querySelector('header');
	const modalElem = document.getElementById('modal-filter');
	const html = modalElem.cloneNode(true);

	modalElem.remove();

	filterModal.fire({
		title: `<strong>${modalElem.dataset.title}</strong>`,
		html,
		onOpen: () => {
			[...html.querySelectorAll('.select:not(.select-init)')].forEach((select) => {
				new CustomSelect(select);
			});

			if (window.matchMedia('(max-width: 993px)').matches) {
				button.classList.toggle('action-close');
				button.removeAttribute('data-filter-modal');
				header.addEventListener('click', () => { filterModal.close(); });
			}
		},
		onClose: () => {
			if (window.matchMedia('(max-width: 993px)').matches) {
				button.classList.toggle('action-close');
			}
		},
		onDestroy: () => {
			[...html.querySelectorAll('.select.select-init')].forEach((select) => {
				select.customSelect.destroy();
			});

			if (window.matchMedia('(max-width: 993px)').matches) {
				button.setAttribute('data-filter-modal', '');
				header.removeEventListener('click', () => { filterModal.close(); });
			}

			document.body.appendChild(html);
		}
	});

	e.preventDefault();
});