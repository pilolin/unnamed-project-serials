import device from 'current-device';
import { hideScroll, showScroll, css } from './../helper';
import { serialInfoPopover, episodeInfoPopover } from './info-popover-templates';

const DESKTOP_POPOVER_WIDTH = 384;
const DESKTOP_POPOVER_OFFSET = 8;

const MOCK_SERIAL = {
	id: 1231,
	name: 'Рик и Морти',
	originalName: 'Rick and Morty',
	rating: {
		kinopoisk: 7.6,
		imdb: 8.4,
	},
	status: 'выходит',
	genres: 'комедия, мелодрама',
	years: 2013,
	amountSeasons: '5 сезонов',
	description: `В центре сюжета — школьник по имени Морти и его дедушка Рик. Морти - самый обычный мальчик, 
								который ничем не отличается от своих сверстников. А вот его дедуля занимается необычными 
								научными исследованиями и зачастую полностью неадекватен. Он может в любое врем...`,
	firstEpisodeUrl: 'https://www.youtube.com/embed/dQw4w9WgXcQ',
	trailer: 'https://www.youtube.com/embed/dQw4w9WgXcQ',
}

/* 
 * new InfoPopover().init(serialElement, {
 *		name,
 *		originalName,
 *		rating: { kinopoisk, imdb },
 *		status,
 *		genres,
 *		years,
 *		amountSeasons,
 *		description,
 *		firstEpisodeUrl,
 *		trailer,
 * })
 */

const MOCK_EPISODE = {
	name: 'Пёс-Газонокосильщик',
	rating: 7.6,
	season: 1,
	episode: 2,
	description: `Во 2 серии 1-го сезона Рик соорудил шлем, делающий собак умнее, который надевают 
								на своего пса Снаффлса. Рик и Морти отправляются в путешествие по снам людей, где 
								встречают Страшного Тэрри.`,
	trailer: 'https://www.youtube.com/embed/dQw4w9WgXcQ',
};


 /* 
 * new InfoPopover().init(episodeElement, {
 *		name,
 *		rating,
 *		season,
 *		episode,
 *		description,
 *		trailer,
 * })
 */

class InfoPopover {
	constructor() {
		this.element;
		this.data;
		this.popover;
		this.deviceType;
		this.backdropElement;
	}

	init(element, data) {
		const documentWidth = document.documentElement.clientWidth;

		this.element = element;
		this.data = data;

		if( device.desktop() && ((documentWidth - this.element.clientWidth) / 2) > DESKTOP_POPOVER_WIDTH ) {
			this.deviceType = 'desktop';
		} else {
			this.deviceType = 'mobile';
		}

		this.createPopover();

		this.open(this.element, this.popover);
	}

	createPopover() {
		this.popover = document.createElement('div');
		this.popover.id = `${this.element.dataset.popoverType}-${this.element.dataset.infoPopoverId}`;
		this.popover.classList.add('info-popover');

		this.popover.dataset.popoverType = this.element.dataset.popoverType;
		this.popover.dataset.deviceType = this.deviceType;

		switch(this.element.dataset.popoverType) {
			case 'serial':
				this.popover.insertAdjacentHTML('afterbegin', serialInfoPopover(this.data, this.deviceType));
				break;
			case 'episode':
				this.popover.insertAdjacentHTML('afterbegin', episodeInfoPopover(this.data, this.deviceType));
				break;
		}

		document.body.appendChild(this.popover);

		this.popover.querySelector('.info-popover__close').addEventListener('click', () => { this.close(); });
	}

	createBackdrop() {
		this.backdropElement = document.createElement('div');

		this.backdropElement.id = 'info-popover-backdrop';

		css(this.backdropElement, {
			transition: '0.2s ease-in-out',
			position: 'fixed',
			top: 0,
			right: 0,
			bottom: 0,
			left: 0,
			background: 'black',
			opacity: 0,
			zIndex: 1062,
		});

		document.body.appendChild(this.backdropElement);

		this.popover.addEventListener('click', (e) => { this.closeClickWithoutModal(e); });

		setTimeout(() => {
			css(this.backdropElement, { opacity: 0.8 });
		}, 50)
	}

	removeBackdrop() {
		this.backdropElement = document.getElementById('info-popover-backdrop');

		this.popover.removeEventListener('click', (e) => { this.closeClickWithoutModal(e); } );
		
		if( this.backdropElement ) {
			css(this.backdropElement, {
				opacity: 0,
				'pointer-events': 'none',
			});
	
			setTimeout(() => {
				this.backdropElement.remove();
			}, 300);
		}
	}

	setPositionDesktop(button, popover) {
		const buttonPos = button.getBoundingClientRect();
		const documentWidth = document.documentElement.clientWidth;

		if( (documentWidth - buttonPos.right - DESKTOP_POPOVER_OFFSET - DESKTOP_POPOVER_WIDTH) > 0 ) {
			css(popover, {
				top: buttonPos.top - 8 + 'px',
				left: buttonPos.right + 'px',
				'padding-left': DESKTOP_POPOVER_OFFSET + 'px'
			});
			popover.dataset.position = 'right';
		} else {
			css(popover, {
				top: buttonPos.top - 8 + 'px',
				left: buttonPos.left - DESKTOP_POPOVER_WIDTH - DESKTOP_POPOVER_OFFSET + 'px',
				'padding-right': DESKTOP_POPOVER_OFFSET + 'px'
			});
			popover.dataset.position = 'left';
		}
	}

	open(button, popover) {
		if (!popover) return;

		popover.setAttribute('aria-hidden', false);

		if(popover.dataset.deviceType === 'desktop') { // desktop
			this.setPositionDesktop(button, popover);
			popover.classList.add('animated', 'fadeIn', 'fast');

			window.addEventListener('resize', () => { this.setPositionDesktop(button, popover) });
		} else { // mobile
			this.createBackdrop();
			hideScroll();
			popover.classList.add('animated', 'fadeInUp', 'fast');
		}
	}
	
	close() {
		const popovers = document.querySelectorAll('.info-popover[aria-hidden="false"]');
		const buttons = document.querySelectorAll('.info-popover_opened[data-info-popover-id]');

		if (!(popovers.length && buttons.length)) return;

		showScroll();
		this.removeBackdrop();

		for (const popover of [...popovers]) {
			popover.classList.remove('fadeIn', 'fadeInUp');

			if(popover.dataset.deviceType === 'desktop') { // desktop
				popover.classList.add('animated', 'fadeOut', 'fast');
			} else { // mobile
				popover.classList.add('animated', 'fadeOutDown', 'fast');
			}
			
			setTimeout(() => {
				popover.classList.remove('animated', 'fadeOut', 'fadeOutDown', 'fast');
				popover.setAttribute('aria-hidden', true);
			}, 500);
		}

		for (const button of [...buttons]) {
			button.classList.remove('info-popover_opened');
		}
	}

	closeClickWithoutModal(event) {
		if( event.target.closest('.info-popover-content') ) return;

		this.close();
	}
}

window.InfoPopover = InfoPopover;

// open popovers in mobile
const InfoPopoverInstance = new InfoPopover();

document.addEventListener('click', (e) => {
	if( !(e.target.closest('[data-info-popover-id]') && ('ontouchstart' in window || window.matchMedia('(max-width: 992px)').matches)) ) return;
	
	const button = e.target.closest('[data-info-popover-id]');
	const popoverType = button.dataset.popoverType;
	const popover = document.querySelector(`#${popoverType}-${button.dataset.infoPopoverId}.info-popover[data-device-type="mobile"]`);

	if( popover && popover.getAttribute('aria-hidden') === 'false' ) return;

	if( button.classList.contains('info-loaded') ) {
		InfoPopoverInstance.open(button, popover)
	} else {
		// load new data
		// popoverType -> episode or serial
		InfoPopoverInstance.init(button, popoverType === 'serial' ? MOCK_SERIAL : MOCK_EPISODE);
		button.classList.add('info-loaded');
	}

	button.classList.add('info-popover_opened');

	e.preventDefault();
});

// open popovers in desktop
document.addEventListener('mouseover', (e) => {
	if( !(e.target.closest('[data-info-popover-id]') && (!('ontouchstart' in window) || window.matchMedia('(min-width: 993px)').matches)) ) return;

	const button = e.target.closest('[data-info-popover-id]');
	const popoverType = button.dataset.popoverType;
	const popover = document.querySelector(`#${popoverType}-${button.dataset.infoPopoverId}.info-popover[data-device-type="desktop"]`);

	if( popover && popover.getAttribute('aria-hidden') === 'false' ) return;

	if( button.classList.contains('info-loaded') ) {
		InfoPopoverInstance.open(button, popover)
	} else {
		// load new data
		// popoverType -> episode or serial
		InfoPopoverInstance.init(button, popoverType === 'serial' ? MOCK_SERIAL : MOCK_EPISODE);
		button.classList.add('info-loaded');
	}

	button.classList.add('info-popover_opened');

	e.preventDefault();
});

// close popover in desktop
document.addEventListener('mousemove', (e) => {
	if( ('ontouchstart' in window) || window.matchMedia('(max-width: 992px)').matches ) return;

	if( e.target.closest('.info-popover_opened[data-info-popover-id]') || e.target.closest('.info-popover[aria-hidden="false"]') ) return;

	const popover = document.querySelector(`.info-popover[aria-hidden="false"][data-device-type="desktop"]`);

	if( !popover ) return;

	InfoPopoverInstance.close();
});