import { сutString } from '../helper';

export function serialInfoPopover(data, deviceType) {
	const trailerHtml = (deviceType === 'mobile') ? `<div class="info-popover__trailer"><iframe src="${data.trailer}" frameborder="0"></iframe></div>` : '';

	return `
		<div class="info-popover-wrap">
			${trailerHtml}
			<button class="info-popover__close"><i class="icon-close"></i></button>
			<div class="info-popover-content">
				<div class="info-popover__header">
					<div>
						${data.years ? `<span>${data.years}</span>` : ''}
						${data.amountSeasons ? `<span>${data.amountSeasons}</span>` : ''}
						${data.status ? `<span>${data.status}</span>` : ''}
					</div>
					<div>
						${data.genres ? `<span>${data.genres}</span>` : ''}
						<div class="service-rating">
							${data.rating && data.rating.kinopoisk ? `<div class="service-rating__item"><i class="icon-kinopoisk"></i><span>${data.rating.kinopoisk}</span></div>` : ''}
							${data.rating && data.rating.imdb ? `<div class="service-rating__item"><i class="icon-imdb"></i><span>${data.rating.imdb}</span></div>` : ''}
						</div>
					</div>
				</div>
				<div class="info-popover__actions">
					<a href="${data.firstEpisodeUrl}" class="btn"><span>1 серия</span></a>
					<button class="btn btn_outline_pink" data-trailer-id="${data.id}"><span><i class="icon-play"></i>Трейлер</span></button>
				</div>
				<h3 class="info-popover__name">${data.name}</h3>
				<div class="info-popover__original-name">${data.originalName}</div>
				<div class="info-popover-description show-more">
					<div class="show-more-wrap" aria-expanded="false" data-height="({desktop:162,mobile:150})">
						${сutString(data.description, 256)}
					</div>
					<a class="show-more__toggle" href="javascript:void(0);">
						<span>Посмотреть все описание</span>
						<i class="icon-duble-chevron-down"></i>
					</a>
				</div>
			</div>
		</div>`;
};

export function episodeInfoPopover(data, deviceType) {
	const trailerHtml = (deviceType === 'mobile') ? `<div class="info-popover__trailer"><iframe src="${data.trailer}" frameborder="0"></iframe></div>` : '';

	return `
		<div class="info-popover-wrap">
			${trailerHtml}
			<button class="info-popover__close"><i class="icon-close"></i></button>
			<div class="info-popover-content">
				<div class="info-popover__header">
					<div class="info-popover__nav"><span>${data.season} сезон</span><span>${data.episode} серия</span></div>
					<div class="info-popover__rating"><i class="icon-star"></i>${data.rating}</div>
				</div>
				<h3 class="info-popover__name">Описание серии <br> «${data.name}»</h3>
				<div class="info-popover-description show-more">
					<div class="show-more-wrap" aria-expanded="false" data-height="({desktop:147,mobile:150})">
						${сutString(data.description, 256)}
					</div>
					<a class="show-more__toggle" href="javascript:void(0);">
						<span>Посмотреть все описание</span>
						<i class="icon-duble-chevron-down"></i>
					</a>
				</div>
			</div>
		</div>`;
};