import Swal from 'sweetalert2/dist/sweetalert2.js';
import SimpleBar from 'simplebar';

const swalModal = Swal.mixin({
	showConfirmButton: false,
	showCloseButton: true,
	closeButtonHtml: '<i class="icon-close"></i>',
	showClass: {
		popup: 'animated fadeInUp fast'
	},
	hideClass: {
		popup: 'animated fadeOutUp fast'
	}
});


// open modal by id
document.addEventListener('click', (e) => {
	const target = e.target;

	if( !(target.closest('a, button') && target.closest('a, button').dataset.modalOpen) ) return;

	const modalId = target.closest('a, button').dataset.modalOpen;
	const modalElem = document.getElementById('modal-' + modalId);
	const title = (modalElem.dataset.title ? `<strong>${modalElem.dataset.title}</strong>` : '') + (modalElem.dataset.subtitle ? `<span>${modalElem.dataset.subtitle}</span>` : '');
	const html = modalElem.cloneNode(true);

	modalElem.remove();

	swalModal.fire({ 
		title, 
		html,
		onOpen: () => {
			[...html.querySelectorAll('.select:not(.select-init)')].forEach((select) => {
				new CustomSelect(select);
			});

			for (const elem of html.querySelectorAll('[data-simplebar]')) {
				const forceVisible = elem.dataset.simplebarForceVisible && elem.dataset.simplebarForceVisible != 'false' ? elem.dataset.simplebarForceVisible : false;
				const autoHide = (elem.dataset.simplebarAutoHide == 'true');

				new SimpleBar(elem, {
					forceVisible,
					autoHide,
				})
			}
		},
		onDestroy: () => {
			[...html.querySelectorAll('.select.select-init')].forEach((select) => {
				select.customSelect.destroy();
			});
		}
	});

	e.preventDefault();
});