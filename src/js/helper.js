export function debounce(f, ms) {
	let isCooldown = false;

  return function() {
    if (isCooldown) return;
    f.apply(this, arguments);
    isCooldown = true;
    setTimeout(() => isCooldown = false, ms);
  };
}

export function сutString(str, maxLength) {
	return str.split(' ').reduce((res, cur) => ((res + ' ' + cur).length <= maxLength ? (res + ' ' + cur) : res)) + '...';
}

export function isTouchDevice() {
	return 'ontouchstart' in window;
}

// lock scroll
export function hideScroll() {
	setTimeout(() => {
		if ( !document.body.hasAttribute('data-body-scroll-fix') ) {
			const scrollPosition = window.pageYOffset || document.documentElement.scrollTop;

			document.body.setAttribute('data-body-scroll-fix', scrollPosition);
			document.body.style.overflow = 'hidden';
			document.body.style.position = 'fixed';
			document.body.style.top = '-' + scrollPosition + 'px';
			document.body.style.left = '0';
			document.body.style.width = '100%';
			document.body.style.paddingRight = getWidthPageScroll() + 'px';
		}
	}, 10 );
}

// unlock scroll
export function showScroll() {
	if ( document.body.hasAttribute('data-body-scroll-fix') ) {
		const scrollPosition = document.body.getAttribute('data-body-scroll-fix');

		document.body.removeAttribute('data-body-scroll-fix');
		document.body.style.overflow = '';
		document.body.style.position = '';
		document.body.style.top = '';
		document.body.style.left = '';
		document.body.style.width = '';
		document.body.style.paddingRight = '';
		window.scroll(0, scrollPosition);
	}
}

export function getWidthPageScroll() {
	const div = document.createElement('div');

	div.style.overflowY = 'scroll';
	div.style.width = '50px';
	div.style.height = '50px';
	document.body.append(div);

	let scrollWidth = div.offsetWidth - div.clientWidth;

	div.remove();

	return scrollWidth;
}

// add styles
export function css(element, styles) {
	for (const name in styles) {
		element.style[name] = styles[name];
	}

	return true;
}

export function camelToKebabCase(str) {
	return str.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase();
}

export function kebabToCamelCase(str){
	const capital = str.split('-').map((item, i) => i ? item[0].toUpperCase() + item.slice(1).toLowerCase() : item);

	return  capital.join('');
}