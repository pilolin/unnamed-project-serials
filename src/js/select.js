import device from 'current-device';

export default class CustomSelect {
	constructor(element) {
		this.element = element;

		if( !this.element ) return;

		this.label = this.element.dataset.label
		this.fieldElement = document.createElement('div');
		this.listElement = document.createElement('div');

		this.select = this.element.querySelector('select');

		this.init();

		this.element.customSelect = {
			// elements
			...this,
			// methods
			open: this.open,
			close: this.close,
			get: this.get,
			set: this.set,
			toggle: this.toggle,
			destroy: this.destroy,
		};
	}

	init() {
		const options = this.element.querySelectorAll('option');

		this.element.classList.add('select-init');

		this.fieldElement.classList.add('select-field');
		this.fieldElement.insertAdjacentHTML(
			'afterbegin', 
			`<div class="select-field__label">${this.label}</div>
			<div class="select-field__value"></div>
			<i class="icon-chevron-down"></i>`
		);

		let selectList = [];

		for (const option of [...options]) {
			selectList.push(`<li data-value="${option.value}">${option.innerHTML}</li>`);
		}

		this.listElement.classList.add('select-list');
		this.listElement.insertAdjacentHTML(
			'afterbegin', 
			`<div class="select-list-wrap" data-simplebar data-simplebar-auto-hide="false">
				<ul>
					${selectList.join('')}
				</ul>
			</div>`
		);

		this.element.appendChild(this.fieldElement);
		this.element.appendChild(this.listElement);

		this.fieldElement.addEventListener('click', () => { this.toggle() });

		this.select.addEventListener('change', (e) => { this.set(e.target.value) });

		this.listElement.addEventListener('click', (e) => { this.selectOption(e.target) });

		document.addEventListener('click', (e) => {
			if ( e.target.closest('.select-init') !== this.element && this.element.getAttribute('aria-expanded') === 'true' ) {
				this.close();
			}
		});
	}

	open() {
		if(device.desktop()) {
			const opened = (this.element.getAttribute('aria-expanded') == 'true');

			if( !opened ) {
				this.element.setAttribute('aria-expanded', true);
			}
		}

		return true;
	}

	close() {
		if(device.desktop()) {
			const opened = (this.element.getAttribute('aria-expanded') == 'true');

			if( opened ) {
				this.element.setAttribute('aria-expanded', false);
			}
		}

		return true;
	}

	toggle() {
		if(device.desktop()) {
			const opened = (this.element.getAttribute('aria-expanded') == 'true');

			if( opened ) {
				this.element.setAttribute('aria-expanded', false);
			} else {
				this.element.setAttribute('aria-expanded', true);
			}
		}

		return true;
	}

	selectOption(targetLi) {
		this.set(targetLi.dataset.value);

		this.close();

		return true;
	}

	get() {
		return this.select.value;
	}

	set(value = '') {		
		const selectFieldValueElement = this.fieldElement.querySelector('.select-field__value');
		const selectedItem = this.element.querySelector('li[aria-selected="true"]');

		const correctValue = [...this.select.querySelectorAll('option')].reduce((result, option) => result || (option.value == value) , false) ? value : '';

		this.select.value = correctValue;

		selectFieldValueElement.innerHTML = this.select.value ? this.select.querySelector(`option[value="${correctValue}"]`).innerHTML : '';

		this.element.setAttribute('aria-selected', correctValue ? true : false);

		if(selectedItem) {
			selectedItem.setAttribute('aria-selected', false);
		}

		this.listElement.querySelector(`li[data-value="${correctValue}"]`).setAttribute('aria-selected', true);

		return true;
	}

	destroy() {
		this.element.classList.remove('select-init');

		this.fieldElement.removeEventListener('click', () => { this.toggle() });

		this.select.removeEventListener('change', () => { this.set() });

		this.listElement.removeEventListener('click', () => { this.selectOption() });

		document.removeEventListener('click', (e) => {
			if ( e.target.closest('.select-init') !== this.element && this.element.getAttribute('aria-expanded') === 'true' ) {
				this.close();
			}
		});

		this.element.customSelect = undefined;
		this.element.querySelector('.select-field').remove();
		this.element.querySelector('.select-list').remove();

		return true;
	}
}