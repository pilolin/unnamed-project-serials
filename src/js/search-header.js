import {hideScroll, showScroll, getWidthPageScroll} from './helper';

// open header search field
document.querySelector('.header-search-field input').addEventListener('focus', openHeaderSearchField);

// close header search field on button
document.querySelector('.header-search__button-close').addEventListener('click', closeHeaderSearchField);

// close header search field on backdrop
document.querySelector('.header-search__backdrop').addEventListener('click', closeHeaderSearchField);

// close header search field on button on keybord "esc"
document.addEventListener('keydown', (e) => {
	if (e.keyCode == 27) {
		closeHeaderSearchField();
	}
});

function openHeaderSearchField() {
	const widthScroll = getWidthPageScroll();

	document.querySelector('.header').classList.add('search-opened');

	if (window.matchMedia('(min-width: 1024px)').matches) {
		document.querySelector('.header-search-results').style.marginLeft = `-${widthScroll/2}px`;
	}

	hideScroll();
}

function closeHeaderSearchField() {
	const header = document.querySelector('.header');
	const inputField = header.querySelector('.header-search-field input')

	document.querySelector('.header').classList.remove('search-opened');
	inputField.blur();

	showScroll();
}