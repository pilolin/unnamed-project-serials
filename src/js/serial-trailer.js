import Swal from 'sweetalert2/dist/sweetalert2.js';
import SnowMoreText from './show-more';

const trailerModal = Swal.mixin({
	showConfirmButton: false,
	showCloseButton: true,
	closeButtonHtml: '<i class="icon-close"></i>',
	showClass: {
		popup: 'animated fadeInUp fast'
	},
	hideClass: {
		popup: 'animated fadeOutUp fast'
	},
	customClass: {
		container: 'swal2-trailer',
	}
});

const MOCK = {
	trailer: 'https://www.youtube.com/embed/dQw4w9WgXcQ',
	props: ['мультфильм, комедия', 'Великобритания', 'выходит', '5 сезонов'],
	rating: {
		kinopoisk: 7.6,
		imdb: 8.4,
	},
	linkFirstEpisode: 'https://www.youtube.com/embed/dQw4w9WgXcQ',
	name: 'Рик и Морти',
	originalName: 'Rick and Morty',
	description: `В центре сюжета - школьник по имени Морти и его дедушка Рик. Морти - самый 
								обычный мальчик, который ничем не отличается от своих сверстников. А вот его 
								дедуля занимается необычными научными исследованиями и зачастую полностью неадекватен. 
								Он может в любое время дня и ночи схватить внука и отправиться вместе с ним в безумные приключения 
								с помощью построенной из разного хлама летающей тарелки, которая способна перемещаться сквозь 
								межпространственный тоннель. Каждый раз эта парочка оказывается в самых неожиданных местах и 
								самых нелепых ситуациях.`,
}

function createModal(data) {
	let props = data.props.map(prop => `<li>${prop}</li>`);

	const modal = document.createElement('div');

	modal.classList.add('modal', 'modal-trailer');
	modal.id = `modal-trailer-${data.id}`;
	
	modal.insertAdjacentHTML('afterbegin', `
		<div class="trailer">
			<div class="trailer-video">
				<iframe src="${data.trailer}" frameborder="0"></iframe>
			</div>
			<div class="trailer-content">
				<div class="trailer-content-top">
					<ul class="trailer__serial-props">${props.join('')}</ul>
					<div class="service-rating">
					<div class="service-rating__item"><i class="icon-kinopoisk"></i><span>${data.rating.kinopoisk}</span></div>
					<div class="service-rating__item"><i class="icon-imdb"></i><span>${data.rating.imdb}</span></div>
				</div>
				</div><a href="${data.linkFirstEpisode}" class="btn full-width"> <span>Смотреть с 1 серии</span></a>
				<h3 class="trailer__serial-name">${data.name}</h3>
				<p class="trailer__serial-original-name">${data.originalName}</p>
				<div class="trailer__serial-description show-more">
					<div aria-expanded="false" data-height="({desktop:147,mobile:150})" class="show-more-wrap">
						${data.description}
					</div>
					<a href="javascript:void(0);" class="show-more__toggle"><span>Посмотреть все описание</span><i class="icon-duble-chevron-down"></i></a>
				</div>
			</div>
		</div>`);

	return modal;
}

function getModalTrailer(trailerId) {
	return new Promise((resolve) => {
		const modalElem = document.getElementById('modal-trailer-' + trailerId);
		let modal;

		if (modalElem) {
			modal = modalElem.cloneNode(true);
			modalElem.remove();
		} else {
			modal = createModal({
				id: trailerId,
				...MOCK
			});
		}

		resolve(modal)
	})
}

document.addEventListener('click', (e) => {
	if (e.target.closest('[data-trailer-id')) {
		getModalTrailer(e.target.dataset.trailerId).then((html) => {
			trailerModal.fire({ 
				html,
				onOpen: () => {
					new SnowMoreText(html.querySelector('.show-more'));
				},
				onDestroy: () => {
					document.body.appendChild(html);
				}
			});
		})
	}
})