import {debounce, css} from './helper';
import { Swiper, Navigation } from 'swiper/swiper.esm.js';

Swiper.use([Navigation]);

window.addEventListener('load', () => {
	if (document.querySelector('.serial-episodes__seasons')) {
		new Swiper('.serial-episodes__seasons', {
			slidesPerView: 'auto',
			freeMode: true,
			freeModeSticky: true,
			navigation: {
				prevEl: '.swiper-button-prev',
				nextEl: '.swiper-button-next',
			}
		});
	}
});

const observerTabs = new MutationObserver((mutations) => {
	mutations.forEach((mutation) => {
		const tabs = mutation.target.closest('[role="tablist"]');
		const isTabSelected = mutation.target.getAttribute('aria-selected') === 'true';

		tabs.querySelector(mutation.target.hash).setAttribute('aria-hidden', !isTabSelected);

		if (tabs.querySelector(mutation.target.hash + ' .swiper-container-initialized')) {
			tabs.querySelector(mutation.target.hash + ' .swiper-container-initialized').swiper.update();
		}

		if (isTabSelected) {
			setPositionBorderActiveTab(mutation.target.closest('[role="tablist"]'));
		}
  });
});

for (const tab of document.querySelectorAll('[role="tab"]')) {
	observerTabs.observe(tab, { attributes: true, childList: false, characterData: false });
}

function setPositionBorderActiveTab(tabs) {
	const tabsList = tabs.firstElementChild;
	const tabsListPos = tabsList.getBoundingClientRect();
	const activeTab = tabs.querySelector('[role="tab"][aria-selected="true"]');
	const activeTabPos = activeTab.getBoundingClientRect();
	const border = tabs.querySelector('.tabs-list__border');

	css(border, {
		top: 'calc(100% - 4px)',
		left: (activeTabPos.left - tabsListPos.left) + 'px',
		width: activeTab.clientWidth + 'px',
	})
}

function allSetPositionBorderActiveTab() {
	const tabsBlocks = document.querySelectorAll('.tabs');

	for (const tabs of tabsBlocks) {
		setPositionBorderActiveTab(tabs);
	}
}

const debAllSetPositionBorderActiveTab = debounce(allSetPositionBorderActiveTab, 150);

window.addEventListener('load', allSetPositionBorderActiveTab);
window.addEventListener('resize', debAllSetPositionBorderActiveTab);

document.addEventListener('click', (e) => {
	if(!e.target.closest('[role="tab"]')) return;
	
	e.preventDefault();

	if(!e.target.closest('[role="tab"][aria-selected="false"]')) return;

	const tabs = e.target.closest('.tabs');

	const targetTab = e.target.closest('[role="tab"][aria-selected="false"]');
	const activeTab = tabs.querySelector(`[role="tab"][aria-selected="true"]`);

	targetTab.setAttribute('aria-selected', 'true');
	activeTab.setAttribute('aria-selected', 'false');

	history.pushState({}, document.title, window.location.pathname + targetTab.hash);
});